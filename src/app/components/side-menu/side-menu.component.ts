import {Component} from '@angular/core';
import {MenuController, NavController} from '@ionic/angular';


@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],

})
export class SideMenuComponent {

  // menuOptions: MenuOption[] = MENU_OPTIONS;

  constructor(private nav: NavController,
              private menu: MenuController,) {
  }

  async onLogoutClick() {
    await this.nav.pop();
    await this.nav.navigateBack(['/login']);
    await this.menu.close();
  }

  async onMenuSelect() {
    await this.menu.close();
  };


}
