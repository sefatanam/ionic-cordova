import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TabsComponent } from './tabs.component';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [TabsComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild([

      {
        path: 'tabs',
        component: TabsComponent,
        children: [
          {
            path: 'home',
            loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
          },
          {
            path: 'profile',
            loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)
          },
          {
            path: 'alerts',
            loadChildren: () => import('./alerts/alerts.module').then(m => m.AlertsModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: 'tabs/home',
        pathMatch: 'full'
      },
      {
        path: '**',
        loadChildren: () => import('./not-found/not-found.module').then(m => m.NotFoundModule)
      },

    ])
  ]
})
export class TabsModule {
}
